<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Motos $model */

$this->title = 'Update Motos: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Motos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="motos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
